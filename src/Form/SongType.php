<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Song;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SongType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,['label' => 'Titre'])
            ->add('url', TextType::class,['label' => 'Hyperlien vers la musique'])
            ->add('evaluation', PercentType::class,['label' => 'Note'])
            ->add('artist',EntityType::class, [
                'label' => 'Artiste',
                'class' => Artist::class,
                'choice_label' => function (Artist $artist) {
                    return $artist->getName();
                }
            ])
            ->add('album',EntityType::class, [
                'label'=>'Album',
                'class' => Album::class,
                'choice_label' => function (Album $album) {
                    return $album->getTitle();
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Song::class,
        ));
    }
}