<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SongRepository")
 */
class Song
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="float", length=255)
     */
    private $evaluation;
    /**
     * @ORM\ManyToOne(targetEntity="Artist")
     */
    private $artist;

    /**
     * @ORM\ManyToOne(targetEntity="Album")
     */
    private $album;

    public function __construct()
    {
        $this->evaluation = 0.0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(String $title)
    {
        $this->title = $title;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl(String $url)
    {
        $this->url = $url;
    }

    public function getEvaluation()
    {
        return $this->evaluation;
    }

    public function setEvaluation(float $evaluation)
    {
        $this->evaluation = $evaluation;
    }

    public function getArtist()
    {
        return $this->artist;
    }

    public function setArtist(Artist $artist)
    {
        $this->artist = $artist;
    }

    public function getAlbum()
    {
        return $this->album;
    }

    public function setAlbum(Album $album)
    {
        $this->album = $album;
    }


}