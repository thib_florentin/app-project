<?php

namespace App\Repository;

use App\Entity\Song;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SongRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Song::class);
    }

    public function findAll(): array
    {
        $qb = $this->createQueryBuilder('s')
            ->orderBy('s.title', 'ASC')
            ->getQuery();

        return $qb->execute();
    }
}