<?php

namespace App\Repository;

use App\Entity\Artist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ArtistRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    /**
     * @return Artist[]
     */
    public function findAll(): array
    {
        $qb = $this->createQueryBuilder('ar')
            ->orderBy('ar.name', 'ASC')
            ->getQuery();

        return $qb->execute();
    }
}