<?php

namespace App\Repository;

use App\Entity\Album;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AlbumRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Album::class);
    }

    public function findAll(): array
    {
        $qb = $this->createQueryBuilder('al')
            ->orderBy('al.title', 'ASC')
            ->getQuery();

        return $qb->execute();
    }
}