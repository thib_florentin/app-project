<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AlbumController extends AbstractController
{

    /**
     * @Route("/albums", name="index_album")
     */
    public function index()
    {
        $albums = $this->getDoctrine()
            ->getRepository(Album::class)
            ->findAll();

        return $this->render('Album/index.html.twig',[
            'albums' => $albums
        ]);
    }

    /**
     * @Route("/albums/add/", name="adding_album")
     */
    public function new(Request $request)
    {
        $album = new Album();
        $form = $this->createForm(AlbumType::class, $album);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($album);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('form.html.twig',[
            'form' => $form->createView(),
            'action' => 'Créarion d\'un nouvel album'
        ]);
    }

    /**
     * @Route("/albums/{id}", name="album_show")
     */
    public function show(Album $album)
    {
        $albumToReturn = $this->getDoctrine()
            ->getRepository(Album::class)
            ->findOneBy(['id'=>$album->getId()]);

        return $this->render('Album/show.html.twig',[
            'album' => $albumToReturn
        ]);
    }

    /**
     * @Route("/album/{id}/remove", name="album_remove")
     */
    public function remove(Album $album)
    {
        $albumToReturn = $this->getDoctrine()
            ->getRepository(Album::class)
            ->findOneBy(['id'=>$album->getId()]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($albumToReturn);
        $entityManager->flush();

        $this->addFlash('success','Artiste supprimé avec succès!');
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/albums/{id}/edit" ,name="album_edit")
     */
    public function update(Request $request, Album $album)
    {
        $albumToGet = $this->getDoctrine()
            ->getRepository(Album::class)
            ->findOneBy(['id'=>$album->getId()]);

        $form = $this->createForm(AlbumType::class, $albumToGet);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($album);
            $entityManager->flush();

            return $this->redirectToRoute('album_show', [
                'id' => $album->getId()
            ]);
        }

        return $this->render('form.html.twig',[
            'form' => $form->createView(),
            'action' => 'Modification d\'un album'
        ]);
    }
}