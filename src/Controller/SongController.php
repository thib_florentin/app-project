<?php

namespace App\Controller;

use App\Entity\Song;
use App\Form\SongType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SongController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $songs = $this->getDoctrine()
            ->getRepository(Song::class)
            ->findAll();

        return $this->render('index.html.twig',[
            'songs' => $songs
        ]);
    }

    /**
     * @Route("/songs/add/", name="adding_song")
     */
    public function new(Request $request)
    {
        $song = new Song();
        $form = $this->createForm(SongType::class, $song);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($song);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('form.html.twig',[
            'form' => $form->createView(),
            'action' => 'Création d\'un nouveau titre'
        ]);
    }

    /**
     * @Route("/songs/{id}", name="song_show")
     */
    public function show(Song $song)
    {
        $songToReturn = $this->getDoctrine()
            ->getRepository(Song::class)
            ->findOneBy(['id'=>$song->getId()]);

        return $this->render('Song/show.html.twig',[
            'song' => $songToReturn
        ]);
    }

    /**
     * @Route("/songs/{id}/remove", name="song_remove")
     */
    public function remove(Song $song)
    {
        $songToReturn = $this->getDoctrine()
            ->getRepository(Song::class)
            ->findOneBy(['id'=>$song->getId()]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($songToReturn);
        $entityManager->flush();

        $this->addFlash('success','Titre supprimé avec succès!');

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/songs/{id}/edit" ,name="song_edit")
     */
    public function update(Request $request, Song $song)
    {
        $songToGet = $this->getDoctrine()
            ->getRepository(Song::class)
            ->findOneBy(['id'=>$song->getId()]);

        $form = $this->createForm(SongType::class, $songToGet);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($song);
            $entityManager->flush();

            return $this->redirectToRoute('song_show', [
                'id' => $song->getId()
            ]);
        }

        return $this->render('form.html.twig',[
            'form' => $form->createView(),
            'action' => 'Modification d\'un titre'
        ]);
    }
}