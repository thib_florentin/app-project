<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Form\ArtistType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArtistController extends AbstractController
{
    /**
     * @Route("/artists", name="index_artist")
     */
    public function index()
    {
        $artists = $this->getDoctrine()
            ->getRepository(Artist::class)
            ->findAll();

        return $this->render('Artist/index.html.twig',[
            'artists' => $artists
        ]);
    }

    /**
     * @Route("/artists/add/", name="adding_artist")
     */
    public function new(Request $request)
    {
        $artist = new Artist();
        $form = $this->createForm(ArtistType::class, $artist);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($artist);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('form.html.twig',[
            'form' => $form->createView(),
            'action' => 'Création d\'un nouvel artiste'
        ]);
    }

    /**
     * @Route("/artists/{id}", name="artist_show")
     */
    public function show(Artist $artist)
    {
        $artistToReturn = $this->getDoctrine()
            ->getRepository(Artist::class)
            ->findOneBy(['id' => $artist->getId()]);

        return $this->render('Artist/show.html.twig',[
            'artist' => $artistToReturn
        ]);
    }

    /**
     * @Route("/artists/{id}/remove", name="artist_remove")
     */
    public function remove(Artist $artist)
    {
        $artistToReturn = $this->getDoctrine()
            ->getRepository(Artist::class)
            ->findOneBy(['id'=>$artist->getId()]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($artistToReturn);
        $entityManager->flush();

        $this->addFlash('success','Artiste supprimé avec succès!');
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/artists/{id}/edit" ,name="artist_edit")
     */
    public function update(Request $request, Artist $artist)
    {
        $artistToGet = $this->getDoctrine()
            ->getRepository(Artist::class)
            ->findOneBy(['id'=>$artist->getId()]);

        $form = $this->createForm(ArtistType::class, $artistToGet);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($artist);
            $entityManager->flush();

            return $this->redirectToRoute('artist_show', [
                'id' => $artist->getId()
            ]);
        }

        return $this->render('form.html.twig',[
            'form' => $form->createView(),
            'action' => 'Modification d\'un artiste'
        ]);
    }
}